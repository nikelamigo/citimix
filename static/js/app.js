"use strict";

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

/* ^^^
 * Глобальные-вспомогательные функции
 * ========================================================================== */

/* ^^^
 * Viewport Height Correction
 *
 * @link https://www.npmjs.com/package/postcss-viewport-height-correction
 * ========================================================================== */
function setViewportProperty() {
  var vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', vh + 'px');
}

window.addEventListener('resize', setViewportProperty);
setViewportProperty(); // Call the fuction for initialisation

/* ^^^
 * Возвращает HTML-код иконки из SVG-спрайта
 *
 * @param {String} name Название иконки из спрайта
 * @param {Object} opts Объект настроек для SVG-иконки
 *
 * @example SVG-иконка
 * getSVGSpriteIcon('some-icon', {
 *   tag: 'div',
 *   type: 'icons', // colored для подключения иконки из цветного спрайта
 *   class: '', // дополнительные классы для иконки
 *   mode: 'inline', // external для подключаемых спрайтов
 *   url: '', // путь до файла спрайта, необходим только для подключаемых спрайтов
 * });
 */

function getSVGSpriteIcon(name, opts) {
  opts = _extends({
    tag: 'div',
    type: 'icons',
    "class": '',
    mode: 'inline',
    url: ''
  }, opts);
  var external = '';
  var typeClass = '';

  if (opts.mode === 'external') {
    external = "".concat(opts.url, "/sprite.").concat(opts.type, ".svg");
  }

  if (opts.type !== 'icons') {
    typeClass = " svg-icon--".concat(opts.type);
  }

  opts["class"] = opts["class"] ? " ".concat(opts["class"]) : '';
  return "\n    <".concat(opts.tag, " class=\"svg-icon svg-icon--").concat(name).concat(typeClass).concat(opts["class"], "\" aria-hidden=\"true\" focusable=\"false\">\n      <svg class=\"svg-icon__link\">\n        <use xlink:href=\"").concat(external, "#").concat(name, "\"></use>\n      </svg>\n    </").concat(opts.tag, ">\n  ");
}
/* ^^^
 * JQUERY Actions
 * ========================================================================== */


$(function () {
  'use strict';
  /**
   * определение существования элемента на странице
   */

  $.exists = function (selector) {
    return $(selector).length > 0;
  };

  function getScrollbarWidth() {
    var outer = document.createElement("div");
    outer.style.visibility = "hidden";
    outer.style.width = "100px";
    outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps

    document.body.appendChild(outer);
    var widthNoScroll = outer.offsetWidth; // force scrollbars

    outer.style.overflow = "scroll"; // add innerdiv

    var inner = document.createElement("div");
    inner.style.width = "100%";
    outer.appendChild(inner);
    var widthWithScroll = inner.offsetWidth; // remove divs

    outer.parentNode.removeChild(outer);
    return widthNoScroll - widthWithScroll;
  }

  ;
  var threedPopup = $('[data-remodal-id=3dpopup]').remodal();
  $('._js-3dpopup').on('click', function (event) {
    event.preventDefault();
    threedPopup.open();
  });

  function numberWithSpaces(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    return parts.join(".");
  }

  $('.masked-input').attr('placeholder', '+7(___) ___-__-__').mask("+7(999) 999-99-99");
  /*sizeCover*/

  ;

  (function () {
    var bgCover = $('[data-section-bg]');

    if (!$.exists(bgCover)) {
      return;
    }

    var cover = function cover() {
      var self = $(this);
      var selfBg = self.data('section-bg');

      if (selfBg) {
        self.css('background-image', 'url(' + selfBg + ')');
      }
    };

    $.each(bgCover, cover);
  })();

  var flag768 = false;
  var flag480 = false;
  var stepsBlock = $('.c-steps').length;
  $(window).on('resize', function () {
    $('select').trigger('refresh');
    mainBannerPadding(); // if(stepsBlock) {
    //   var mapWidth  = $('body').outerWidth();
    //   console.log(mapWidth);
    //   $('.c-steps__buildings img').mapster('resize', mapWidth);
    // }

    if (window.matchMedia("(max-width:768px)").matches) {
      if (!flag768) {
        if ($('.c-apartments__content-layout-button').length) {
          $('.c-apartments__content-layout-button').appendTo('.c-apartments__content-layout--right');
        }

        flag768 = true;
      }
    } else {
      if (flag768) {
        if ($('.c-apartments__content-layout-button').length) {
          $('.c-apartments__content-layout-button').appendTo('.c-apartments__content-layout--left');
        }

        flag768 = false;
      }
    } // Удалить при заливке


    if (window.matchMedia("(max-width:480px)").matches) {
      if (!flag480) {
        if ($('.c-mortgage').length) {
          $('.c-mortgage__banks-elem').each(function (index, el) {
            if (index > 1) {
              $(el).hide();
            }
          });
        }

        flag480 = true;
      }
    } else {
      if (flag480) {
        if ($('.c-mortgage').length) {
          $('.c-mortgage__banks-elem').each(function (index, el) {
            $(el).show();
          });
        }

        flag480 = false;
      }
    }
  }).trigger('resize'); // function commaDot(str) {
  //   var str = str.toString().split('.');
  //   str[0]  = str[0].replace(/,/g, '.');
  //   return str.join('.');
  // }
  // function dotComma(str) {
  //   var str = str.toString().replace(/./g, ',');
  //   return str;
  // }

  $('.app-footer__menu > li, .app-footer__design-wrap > li').each(function (index, el) {
    if ($(this).find('>ul').length) {
      $(this).find('>a').append('<span class="menu-trigger"><svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.81323 5.6762L1.76913 0.176209L0.474 1.35479L5.22289 5.67627L0.474131 9.99764L1.76928 11.1762L7.81323 5.6762Z"></path></svg></span>');
    }
  });
  $(document).on('click', '.app-footer__menu .menu-trigger', function (event) {
    event.preventDefault();
    $(this).closest('li').toggleClass('open').find('>ul').slideToggle();
  });
  var maxHeight = $('.app-header__middle').offset().top;
  var blockHeight = $('.app-header__middle').outerHeight() + maxHeight;
  var position = $(window).scrollTop();

  if (!window.matchMedia("(max-width: 1200px)").matches && position > maxHeight) {
    $(".app-header").addClass('scroller');
  } else {
    $(".app-header").removeClass('scroller');
  }

  $(window).on('scroll', function () {
    if (!window.matchMedia("(max-width: 1200px)").matches) {
      var old_scroll = $(window).scrollTop();

      if (old_scroll > maxHeight) {
        $(".app-header").addClass('scroller');
      } else {
        $(".app-header").removeClass('scroller');
      }
    } else {
      var scroll = $(window).scrollTop();

      if (scroll > position || maxHeight >= scroll) {
        $(".app-header").removeClass('upscroll');
      } else {
        $(".app-header").addClass('upscroll');
      }

      if (scroll > blockHeight) {
        $('.app-header').addClass('not-visible').removeClass('visible');
      } else {
        $('.app-header').removeClass('not-visible').addClass('visible');
      }

      position = scroll;
    }
  }); // когда шапка уедет за пределы видимости вещать на нее fixed с -100%

  $('.c-apartment__filter-item-button').on('click', function (event) {
    event.preventDefault();
    $(this).toggleClass('active');
  });
  $('.custom-range').each(function (index, el) {
    var $this = $(this),
        dataStep = $this.data('step'),
        dataDecimals = $this.data('decimals'),
        $lower = $this.closest('.custom-range').find('.custom-range__low'),
        $upper = $this.closest('.custom-range').find('.custom-range__high'),
        inputLowVal = $lower.val().replace(',', '.'),
        inputHighVal = $upper.val().replace(',', '.');
    var inputRangeMax = $upper.data('val').toString().replace(',', '.');
    var inputRangeMin = $lower.data('val').toString().replace(',', '.');
    var arr = [Number(inputRangeMin), Number(inputRangeMax)];
    $lower.on('input', function (event) {
      this.value = this.value.replace(/[^0-9,.]/g, '');
      var value = $(event.target).val().replace(',', '.');
      slider.val([value, null]);
    });
    $upper.on('input', function (event) {
      this.value = this.value.replace(/[^0-9,.]/g, '');
      var value = $(event.target).val().replace(',', '.');
      slider.val([null, value]);
    });
    var slider = $this.find('.custom-range__block').noUiSlider({
      start: [inputLowVal, inputHighVal],
      connect: true,
      behaviour: 'drag-tap',
      format: wNumb({
        decimals: dataDecimals
      }),
      range: {
        'min': [arr[0]],
        'max': [arr[1]]
      },
      step: dataStep
    }); // $lower.on('input', function(event) {
    // 	slider.val([$(event.target).val(), null]);
    // });
    // $upper.on('input', function(event) {
    //   slider.val([null, $(event.target).val()]);
    // });
    // slider.Link('lower').to($lower);
    // slider.Link('upper').to($upper);

    slider.on('set', function (values, handle) {
      var lowVal = parseFloat(slider.val()[0]);

      if (lowVal == inputRangeMin) {
        $this.addClass('custom-range__border');
      } else {
        $this.removeClass('custom-range__border');
      }
    });
    slider.on('slide', function (values, handle) {
      $lower.val(handle[0].replace('.', ','));
      $upper.val(handle[1].replace('.', ','));
      var lowVal = parseFloat(slider.val()[0]);

      if (lowVal == inputRangeMin) {
        $this.addClass('custom-range__border');
      } else {
        $this.removeClass('custom-range__border');
      }
    });
  });
  $('.custom-range--one').each(function (index, el) {
    var $this = $(this),
        dataStep = $this.data('step'),
        dataDecimals = $this.data('decimals'),
        $upper = $this.closest('.custom-range--one').find('.custom-range__high'),
        inputHighVal = $upper.val(),
        inputRangeMax = $upper.data('val').toString().replace(',', '.');
    $upper.val(numberWithSpaces(inputHighVal));
    inputHighVal = inputHighVal.replace(',', '.');
    inputRangeMax = inputRangeMax.replace(',', '.');
    $upper.on('input', function (event) {
      this.value = numberWithSpaces(this.value.replace(/[^0-9,.]/g, ''));
      var value = $(event.target).val().replace(',', '.');
      slider.val(value);
    });
    var slider = $this.find('.custom-range__block').noUiSlider({
      start: inputHighVal,
      connect: 'lower',
      behaviour: 'drag-tap',
      format: wNumb({
        decimals: dataDecimals
      }),
      range: {
        'min': 0,
        'max': Number(inputRangeMax)
      },
      step: dataStep
    }); // $upper.on('input', function(event) {
    //   var value = $(event.target).val().replace(',','.'); 
    //   slider.val(value);
    // });

    slider.on('slide', function (values, handle) {
      $upper.val(numberWithSpaces(handle.replace('.', ',')));
      var lowVal = parseFloat(slider.val()[0]);

      if (lowVal == 0) {
        $this.addClass('custom-range__border');
      } else {
        $this.removeClass('custom-range__border');
      }
    }); // slider.Link('lower').to($lower);
    // slider.Link('upper').to($upper);
  });

  if ($('.c-apartment--selectbypar').length) {
    var filterPopup = $('[data-remodal-id=filterRemodal]').remodal();
    var filterHtml = $(this).find('.c-apartment__block').clone(true);
    $('[data-remodal-id="filterRemodal"] .remodal-content').html(filterHtml);
    $('._js-filter-remodal').on('click', function (event) {
      event.preventDefault();
      filterPopup.open();
      $('select').trigger('refresh');
    });
  }

  ;

  (function () {
    $.preventScrolling = function (selector, options) {
      // запрещаем прокрутку страницы при прокрутке элемента
      var defaults = {
        classes: {
          scrolled: 'is-scrolled',
          onTop: 'is-onTop',
          onBottom: 'is-onBottom'
        },
        onTop: function onTop() {},
        onBottom: function onBottom() {}
      };
      var options = $.extend({}, defaults, options);
      var scroller = $(selector);
      scroller.on('scroll', function () {
        if (scroller.scrollTop() == 0) {
          scroller.addClass(options.classes.onTop).removeClass(options.classes.onBottom);
        }

        if (scroller.scrollTop() == scroller[0].scrollHeight - scroller.height()) {
          scroller.removeClass(options.classes.onTop).addClass(options.classes.onBottom);
        }
      });

      if (scroller[0].scrollHeight > scroller.height()) {
        scroller.addClass('with-scroll');
      } else {
        scroller.removeClass('with-scroll');
      }

      $(window).on('resize', function () {
        if (scroller[0].scrollHeight > scroller.height()) {
          scroller.addClass('with-scroll');
        } else {
          scroller.removeClass('with-scroll');
        }
      });
      scroller.off('mousewheel DOMMouseScroll').on('mousewheel DOMMouseScroll', function (e) {
        var scrollTo = null;

        if (e.type == 'mousewheel') {
          scrollTo = e.originalEvent.wheelDelta * -1;
        } else if (e.type == 'DOMMouseScroll') {
          scrollTo = 40 * e.originalEvent.detail;
        }

        if (scrollTo && scroller[0].scrollHeight > scroller.height()) {
          e.stopPropagation();
          e.preventDefault();
          $(this).scrollTop(scrollTo + $(this).scrollTop());
        }
      });
    };
  })();
  /**
     * Simple menu
     * @version 1.0.0-beta.1
     */


  ;

  (function ($) {
    $.fn.simpleMenu = function (options) {
      var settings = $.extend(true, {
        timing: 300,
        topMargin: -6,
        menu: {
          list: 'ul',
          item: 'li',
          trigger: 'a'
        },
        classes: {
          opened: 'opened',
          active: 'active',
          used: 'used'
        },
        attrs: {
          opened: {
            key: 'opened',
            "true": 'true',
            "false": 'false'
          }
        }
      }, options);
      var $this = this;
      var $trigers = $this.find(settings.menu.list).parent(settings.menu.item).find('> ' + settings.menu.trigger);
      $trigers.on('click', function (event) {
        event.preventDefault();
        var $list = $(this).parent(settings.menu.item).find('> ' + settings.menu.list);
        $list.css({
          display: 'block'
        });

        if ($list.parent(settings.menu.item).hasClass(settings.classes.opened)) {
          $list.stop().animate({
            marginTop: -($list.outerHeight(true) - settings.topMargin)
          }, settings.timing, function () {
            $list.attr(settings.attrs.opened.key, settings.attrs.opened["false"]).addClass(settings.classes.used).parent(settings.menu.item).removeClass(settings.classes.opened);
          });
        } else {
          if (!$list.hasClass(settings.classes.used)) {
            $list.css({
              marginTop: -($list.outerHeight(true) - settings.topMargin)
            }).addClass(settings.classes.used);
          }

          $list.parent(settings.menu.item).addClass('opening').end().stop().animate({
            marginTop: 0 + settings.topMargin
          }, settings.timing, function () {
            $list.attr(settings.attrs.opened.key, settings.attrs.opened["true"]).parent(settings.menu.item).removeClass('opening').end().addClass(settings.classes.used).parent(settings.menu.item).addClass(settings.classes.opened);
          });
        }
      });
    };
  })(jQuery);
  /**
     * Мобильное меню сайта
     */


  var asideMenuBtn = $('.l-aside-menu-btn');
  var asideMenu = $('.l-aside-menu');
  var asideHead = $('.l-aside-menu__head');
  var asideMenuContent = $('.l-aside-menu__content');
  var asideMenuScroller = $('.l-aside-menu__scroller-content');
  var asideMenuFoot = $('.l-aside-menu__foot');

  function openAsideMenu() {
    asideMenuBtn.addClass('js-opening');
    asideMenu.addClass('js-animate js-opening');
    $('html').addClass('menu-fixed');
    $('body').addClass('menu-fixed').css('padding-right', getScrollbarWidth() + 'px');
  }

  function closeAsideMenu() {
    asideMenuBtn.removeClass('js-opening');
    asideMenu.removeClass('js-animate');
    setTimeout(function () {
      asideMenu.removeClass('js-opening');
      $('html').removeClass('menu-fixed');
      $('body').removeClass('menu-fixed').css('padding-right', 0);
    }, 150);
  }

  asideMenuBtn.on('pointerup', function (event) {
    event.preventDefault();

    if ($(this).hasClass('js-opening')) {
      closeAsideMenu();
    } else {
      openAsideMenu();
    }
  });
  /**
     * запрещаем прокрутку страницы при прокрутке бокового-мобильного
     */

  $.preventScrolling($('.l-aside-menu__scroller'));

  if ($.exists('.app-header__icons')) {
    var newMainMenu = $('.app-header__icons').clone();
    newMainMenu.addClass('app-header__icons--adaptive').appendTo(asideMenuScroller);
  }
  /**
     * Клонирование верхнего-левого меню в боковое-мобильное
     */


  if ($.exists('.app-header__menu')) {
    var newMainMenu = $('.app-header__menu').clone();
    newMainMenu.removeClass('app-header__menu').addClass('aside-nav-list aside-nav-list__menu').appendTo(asideMenuScroller);
  }
  /*Добавление стрелочек для li*/


  $.each(asideMenuScroller.find('li'), function (index, element) {
    if ($(element).find('ul').length) {
      var triggerIcon = ['<div class="svg-icon svg-icon--angle-down">', '<svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">', '<path d="M7.81323 5.6762L1.76913 0.176209L0.474 1.35479L5.22289 5.67627L0.474131 9.99764L1.76928 11.1762L7.81323 5.6762Z" fill="black"/>', '</svg>', '</div>'].join('');
      var subMenuTrigger = $('<div class="sub-menu-trigger">' + triggerIcon + '</div>');
      $(element).addClass('is-has-child').append(subMenuTrigger);
    }
  });

  if ($.exists('.aside-nav-list')) {
    $('.aside-nav-list').simpleMenu({
      timing: 500,
      menu: {
        trigger: '.sub-menu-trigger'
      }
    });
  }

  if ($('.b-stories__swiper').length) {
    var storiesSwiper = new Swiper(".b-stories__swiper", {
      slidesPerView: "auto",
      spaceBetween: 30
    });
    var remodalSwiper = new Swiper("[data-remodal-id='extendedStories'] .swiper", {
      navigation: {
        nextEl: "[data-remodal-id='extendedStories'] .swiper-button-next",
        prevEl: "[data-remodal-id='extendedStories'] .swiper-button-prev"
      },
      autoplay: {
        delay: 15000,
        disableOnInteraction: false
      }
    });
    $('.remodal .b-storie video').each(function (index, el) {
      var itemClass = 'palyer-' + index;
      $(this).addClass(itemClass);
      var player = new Plyr('.' + itemClass, {
        autoplay: false
      });
      remodalSwiper.on('slideChange', function () {
        player.pause();
        $('.b-storie__fake-video-length').removeClass('active');
        $('.fake-video-length-' + remodalSwiper.activeIndex).addClass('active');
        $('.palyer-' + remodalSwiper.activeIndex).get(0).play();
      });
      $(document).on('closing', '.remodal', function (e) {
        player.pause();
        $('.b-storie__fake-video-length').removeClass('active');
      });
    });
    $('.b-storie__fake-video-length').each(function (index, el) {
      var itemClass = 'fake-video-length-' + index;
      $(this).addClass(itemClass);
    });
    var extendedStories = $('[data-remodal-id=extendedStories]').remodal();
    $('.b-stories .b-storie__play').on('click', function (event) {
      event.preventDefault();
      extendedStories.open(); // console.log($(this).closest('.b-storie').index());

      remodalSwiper.slideTo($(this).closest('.b-storie').index());

      if ($('.palyer-' + $(this).closest('.b-storie').index()).length) {
        $('.palyer-' + $(this).closest('.b-storie').index()).get(0).play();
      }

      if ($('.fake-video-length-' + $(this).closest('.b-storie').index()).length) {
        $('.fake-video-length-' + $(this).closest('.b-storie').index()).addClass('active');
      }
    });
  }

  if ($('.c-build__swiper').length) {
    var buildSwiper = new Swiper(".c-build__swiper", {
      slidesPerView: "auto",
      spaceBetween: 10,
      breakpoints: {
        1025: {
          slidesPerView: 2,
          spaceBetween: 30
        }
      }
    });
    var buildGallery = $('[data-remodal-id=buildGallery]').remodal();
    $('.c-build__swiper-item').on('click', function (event) {
      event.preventDefault();
      var popupGallery = $(this).find('.c-popup').clone();
      $('[data-remodal-id="buildGallery"] .remodal-content').html(popupGallery);
      var popupSwiper = new Swiper(".c-popup", {
        navigation: {
          nextEl: ".c-popup .swiper-button-next",
          prevEl: ".c-popup .swiper-button-prev"
        }
      });
      buildGallery.open();
    });
    $(document).on('closed', '.remodal[data-remodal-id=buildGallery]', function (e) {
      $('[data-remodal-id="buildGallery"] .remodal-content').html('');
    });
  }

  var callbackPopup = $('[data-remodal-id=callbackpopup]').remodal();
  $('._js-callbackpopup').on('click', function (event) {
    event.preventDefault();
    callbackPopup.open();
  });
  $('.c-callbackpopup__close').on('click', function (event) {
    event.preventDefault();
    callbackPopup.close();
  });

  if ($('.c-company-block__swiper').length) {
    var buildSwiper = new Swiper(".c-company-block__swiper", {
      navigation: {
        nextEl: ".c-company-block__navs .swiper-button-next",
        prevEl: ".c-company-block__navs .swiper-button-prev"
      }
    });
  }

  if ($('.c-company-slider__swiper').length) {
    var buildSwiper = new Swiper(".c-company-slider__swiper", {
      slidesPerView: "auto",
      spaceBetween: 10,
      breakpoints: {
        481: {
          spaceBetween: 30
        }
      }
    });
  }

  if ($('.c-compare-slider__block').length) {
    var buildSwiper = new Swiper(".c-compare-slider__block", {
      slidesPerView: 3,
      spaceBetween: 0,
      breakpoints: {
        0: {
          slidesPerView: 1
        },
        769: {
          slidesPerView: 2
        },
        1201: {
          slidesPerView: 3
        }
      },
      navigation: {
        nextEl: ".c-compare-slider__slider-navs .swiper-button-next",
        prevEl: ".c-compare-slider__slider-navs .swiper-button-prev"
      }
    }); // compareHeights();

    $('.c-compare-slider__item-compare-block').on('click', function (event) {
      event.preventDefault();
      $(this).toggleClass('active');
    });
  }

  function compareHeights() {
    $('.c-compare-slider__item-row--price').css('height', 'auto');
    var priceMaxHeight = 0;
    var mortgageMaxHeight = 0;
    var settMaxHeight = 0;
    var areaMaxHeight = 0;
    var floarMaxHeight = 0;
    $('.c-compare-slider__item').each(function (index, el) {
      var $this = $(this);
      var priceHeight = $this.find('.c-compare-slider__item-row--price').outerHeight();

      if (priceHeight > priceMaxHeight) {
        priceMaxHeight = priceHeight;
      }
    });
    $('.c-compare-slider__item-row--price').css('height', priceMaxHeight + 'px');
  }

  (function () {
    if (!$('#contactsMap').length) {
      return;
    }

    var myMap;
    var myPlacemark;
    ymaps.ready(init);

    function init() {
      // Создание экземпляра карты.
      myMap = new ymaps.Map('contactsMap', {
        center: [55.664334, 37.465928],
        zoom: 15,
        controls: []
      });
      myMap.behaviors.disable('scrollZoom');
      myPlacemark = new ymaps.Placemark([55.664334, 37.465928], {
        hintContent: ''
      }, {
        iconLayout: 'default#image',
        iconImageHref: '../static/img/map1.png',
        iconImageSize: [138, 138]
      });
      myMap.geoObjects.add(myPlacemark);
    }
  })();

  if ($('.c-content__progress-bar-per').length) {
    $('.c-content__progress-bar-per').each(function (index, el) {
      var percent = Number($(this).html());
      $(this).closest('.c-content__progress-bar').find('.c-content__progress-bar-line').css('width', percent + '%');
    });
  }

  $('.c-flat__add-favorites,.c-flat-row__icon-item').on('click', function (event) {
    event.preventDefault();
    $(this).toggleClass('active');
  });

  if ($('.c-insta-gallery').length) {
    var leftMargin = $('.c-insta-gallery__title').offset().left;
    $('.c-insta-gallery__items').css('margin-left', leftMargin + 'px');
    $(window).on('resize', function (event) {
      leftMargin = $('.c-insta-gallery__title').offset().left;
      $('.c-insta-gallery__items').css('margin-left', leftMargin + 'px');
    });
    $('.c-insta-gallery__items').addClass('owl-carousel').owlCarousel({
      margin: 30,
      nav: false,
      dots: false,
      autoWidth: true,
      mouseDrag: true,
      touchDrag: true
    });
  }

  if ($('.c-lot__sliders-wrap').length) {
    $('.c-lot__sliders').each(function (index, el) {
      var indexSlider = index;
      $(this).addClass('slider' + indexSlider);
      var swiper = new Swiper('.c-lot__sliders.slider' + indexSlider + ' .c-lot__thumb-slider', {
        direction: "vertical",
        slidesPerView: 4,
        watchSlidesProgress: true,
        watchSlidesVisibility: true,
        watchOverflow: true,
        spaceBetween: 18,
        breakpoints: {
          0: {
            slidesPerView: 3
          },
          1360: {
            slidesPerView: 4
          }
        }
      });
      var swiper2 = new Swiper('.c-lot__sliders.slider' + indexSlider + ' .c-lot__main-slider', {
        watchOverflow: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        preventInteractionOnTransition: true,
        navigation: {
          nextEl: ".c-lot__sliders.slider" + indexSlider + " .swiper-button-next",
          prevEl: ".c-lot__sliders.slider" + indexSlider + " .swiper-button-prev"
        },
        thumbs: {
          swiper: swiper
        },
        effect: 'fade',
        fadeEffect: {
          crossFade: true
        }
      });
    });
  }

  $('.c-lot__fav').on('click', function (event) {
    event.preventDefault();
    $(this).toggleClass('active');
  });
  $('.c-lot__slider-link a').on('click', function (event) {
    event.preventDefault();
    var $this = $(this);
    var id = $this.attr('href');
    console.log(id);

    if ($this.hasClass('active')) {
      return;
    } else {
      $('.c-lot__sliders').removeClass('active');
      $('.c-lot__slider-link a').removeClass('active');
      $this.addClass('active');
      $(id).addClass('active');
    }
  });

  if ($('.c-lotslider__block').length) {
    var lotSlider = new Swiper(".c-lotslider__block", {
      slidesPerView: 3,
      spaceBetween: 27,
      navigation: {
        nextEl: ".c-lotslider__slider-navs .swiper-button-next",
        prevEl: ".c-lotslider__slider-navs .swiper-button-prev"
      },
      breakpoints: {
        0: {
          slidesPerView: 1,
          spaceBetween: 20
        },
        769: {
          slidesPerView: 2,
          spaceBetween: 20
        },
        1321: {
          slidesPerView: 3,
          spaceBetween: 27
        }
      }
    });
  }

  function mainBannerPadding() {
    $('.c-main-banner').each(function (index, el) {
      var $this = $(this),
          imgHeight = $this.find('.c-main-banner__img').height(),
          contentOffset = $this.find('.c-main-banner__content').position().top,
          contentHeight = $this.find('.c-main-banner__content').outerHeight();
      $this.css('padding-bottom', contentOffset + contentHeight - imgHeight + 'px');
    });
  } // $('.c-main-slider__items').addClass('owl-carousel').owlCarousel({
  // 	items          : 1,
  // 	autoplay       : false,
  // 	loop           : true,
  // 	mouseDrag      : false,
  // 	touchDrag      : true,
  // 	animateOut     : 'fadeOut',
  // 	nav            : true,
  // 	navText        : ['<svg xmlns="http://www.w3.org/2000/svg" width="14" height="26" viewBox="0 0 14 26" fill="none"><path d="M1.77036 25.3287C1.59671 25.5024 1.37966 25.5892 1.1409 25.5892C0.90214 25.5892 0.685085 25.5024 0.511442 25.3287C0.164155 24.9814 0.164155 24.4171 0.511442 24.0698L11.5812 13L0.511442 1.93027C0.164155 1.58298 0.164155 1.01864 0.511442 0.671354C0.858729 0.324067 1.42307 0.324067 1.77036 0.671354L13.4696 12.3706C13.8169 12.7179 13.8169 13.2822 13.4696 13.6295L1.77036 25.3287Z"/></svg>','<svg xmlns="http://www.w3.org/2000/svg" width="14" height="26" viewBox="0 0 14 26" fill="none"><path d="M1.77036 25.3287C1.59671 25.5024 1.37966 25.5892 1.1409 25.5892C0.90214 25.5892 0.685085 25.5024 0.511442 25.3287C0.164155 24.9814 0.164155 24.4171 0.511442 24.0698L11.5812 13L0.511442 1.93027C0.164155 1.58298 0.164155 1.01864 0.511442 0.671354C0.858729 0.324067 1.42307 0.324067 1.77036 0.671354L13.4696 12.3706C13.8169 12.7179 13.8169 13.2822 13.4696 13.6295L1.77036 25.3287Z""/></svg>'],
  // 	dots           : false
  // });


  var mainSlider = new Swiper(".c-main-slider__items", {
    navigation: {
      nextEl: ".c-main-slider__navs .swiper-next",
      prevEl: ".c-main-slider__navs .swiper-prev"
    }
  });

  (function () {
    if (!$('#mapBlock').length) {
      return;
    }

    var myMap;
    var myPlacemark;
    var myMapWithoutPopup = $('#mapBlock').hasClass('c-map__block--without-popup');
    var mapPopup = $('[data-remodal-id=map]').remodal();
    ymaps.ready(init);

    function init() {
      // Создание экземпляра карты.
      myMap = new ymaps.Map('mapBlock', {
        center: [55.664334, 37.465928],
        zoom: 15,
        controls: []
      });
      myMap.behaviors.disable('scrollZoom');
      myPlacemark = new ymaps.Placemark([55.664334, 37.465928], {
        hintContent: ''
      }, {
        iconLayout: 'default#image',
        iconImageHref: '../static/img/map1.png',
        iconImageSize: [138, 138]
      });
      myMap.geoObjects.add(myPlacemark); // var balloonLayout = ymaps.templateLayoutFactory.createClass(
      //             '<div class="b-marker">'
      //             +'<div class="b-marker__item">'
      //             +'<div class="svg-icon svg-icon--location" aria-hidden="true"><svg class="svg-icon__link"><use xlink:href="#location"></use></svg></div>'
      //             +'{{ properties.title }}'
      //             +'</div>'
      //             +'<div class="b-marker__item">'
      //             +'<div class="svg-icon svg-icon--clock" aria-hidden="true"><svg class="svg-icon__link"><use xlink:href="#clock"></use></svg></div>'
      //             +'{{ properties.time }}'
      //             +'</div>'
      //             +'<div class="b-marker__item">'
      //             +'<div class="svg-icon svg-icon--phone" aria-hidden="true"><svg class="svg-icon__link"><use xlink:href="#phone"></use></svg></div>'
      //             +'<a href="tel:{{ properties.phonen }}">{{ properties.phone }}</a>'
      //             +'</div>'
      //             +'<a href="{{ properties.detail }}" class="btn">'
      //             +'Как добраться'
      //             +'</a>'
      //             +'</div>'
      //         , {
      //             build: function () {
      //                 this.constructor.superclass.build.call(this);
      //                 this._$element = $('.contacts__marker', this.getParentElement());
      //                 this._$element.find('.close')
      //                 .on('click', $.proxy(this.onCloseClick, this));
      //             },
      //             onCloseClick: function (e) {
      //                 e.preventDefault();
      //                 this.events.fire('userclose');
      //             },
      //             getShape: function () {
      //                 var el = this.getElement(),
      //                     result = null;
      //                 if (el) {
      //                     var firstChild = el.firstChild;
      //                     result = new ymaps.shape.Rectangle(
      //                         new ymaps.geometry.pixel.Rectangle([
      //                             [0, 0],
      //                             [firstChild.offsetWidth, firstChild.offsetHeight]
      //                         ])
      //                     );
      //                 }
      //                 return result;
      //             }
      //         }
      //     );

      function addCollection(index, el) {
        var $this = $(el);
        var collection = new ymaps.GeoObjectCollection({}, {
          preset: ""
        });
        var icon = $this.data('icon');
        var hoverIcon = $this.data('hover-icon');
        $this.find('.c-map__block-popup-point').each(function (index, el) {
          var latLang = $(el).data('coords').split(',');
          var imgSrc = $(el).data('img');
          var title = $(el).data('title');
          var description = $(el).data('description');
          var link = $(el).data('link');
          var placemark = new ymaps.Placemark([Number(latLang[0]), Number(latLang[1])], {
            title: title,
            description: description,
            imgSrc: imgSrc,
            link: link,
            defaultIcon: icon,
            hoverIcon: hoverIcon
          }, {
            //balloonLayout: balloonLayout,
            //balloonPanelMaxMapArea: 0,
            // hintLayout: HintLayout,
            hideIconOnBalloonOpen: false,
            iconLayout: 'default#imageWithContent',
            iconImageHref: icon,
            iconImageSize: [42, 50],
            iconImageOffset: [0, 0]
          });
          collection.add(placemark);

          if (!myMapWithoutPopup) {
            placemark.events.add('mouseenter', function (e) {
              var hoverIcon = e.get('target').properties.get('hoverIcon');
              e.get('target').options.set('iconImageHref', hoverIcon);
            });
            placemark.events.add('mouseleave', function (e) {
              var defaultIcon = e.get('target').properties.get('defaultIcon');
              e.get('target').options.set('iconImageHref', defaultIcon);
            });
            placemark.events.add('click', function (e) {
              var popup = $('.c-map__placemark-popup');
              var title = e.get('target').properties.get('title');
              var imgSrc = e.get('target').properties.get('imgSrc');
              var description = e.get('target').properties.get('description');
              var link = e.get('target').properties.get('link');
              popup.find('.c-map__placemark-popup-title').text(title);
              popup.find('.c-map__placemark-popup-img img').attr('src', imgSrc);
              popup.find('.c-map__placemark-popup-description').text(description);
              popup.find('.c-map__placemark-popup-link > a').attr('href', link);

              if (window.matchMedia("(max-width: 1200px)").matches) {
                var placemarkPopup = popup.clone();
                $('[data-remodal-id="map"] .remodal-content').html(placemarkPopup);
                mapPopup.open();
              } else {
                popup.addClass('active');
              }
            });
          }
        });
        myMap.geoObjects.add(collection);
        $this.on('click', function (event) {
          event.preventDefault();

          if ($(this).hasClass('disabled')) {
            $(this).removeClass('disabled');
            myMap.geoObjects.add(collection);
          } else {
            $(this).addClass('disabled');
            myMap.geoObjects.remove(collection);
          }
        });
      }

      $('.c-map__block-popup-items.active .c-map__block-popup-item').each(addCollection);
      $('.c-map__link').on('click', function (event) {
        event.preventDefault();
        var $this = $(this);

        if ($this.hasClass('active')) {
          return;
        }

        myMap.geoObjects.removeAll();
        myMap.geoObjects.add(myPlacemark);
        var role = $this.data('role');
        $('.c-map__link').removeClass('active');
        $this.addClass('active');
        $('.c-map__block-popup-items.active .c-map__block-popup-item').off('click');
        $('.c-map__block-popup-items').removeClass('active');
        $('.c-map__placemark-popup').removeClass('active');
        $('.c-map__block-popup-items[data-role="' + role + '"]').addClass('active');
        $('.c-map__block-popup-items.active .c-map__block-popup-item').each(addCollection); // myMap.geoObjects.each(function(obj, i){
        //     if (obj instanceof ymaps.Placemark) {
        //         return
        //     }
        //     myMap.geoObjects.remove(obj);
        // });
      });
    }

    $(document).on('closed', '.remodal[data-remodal-id=map]', function (e) {
      $('[data-remodal-id="map"] .remodal-content').html('');
    });
  })();

  if ($('.c-mortgage').length) {
    var sortRow = $('.c-mortgage').find('.c-mortgage__sort-row').clone(true).addClass('c-mortgage__sort-row--adaptive');
    $('.c-mortgage').find('.c-mortgage__main-layout-item--items').prepend(sortRow);
  }

  $('select').not('.no-styler').styler();
  var PAGE = $('html, body');
  var pageScroller = $('.page-scroller');
  var inMemoryClass = 'page-scroller--memorized';
  var isVisibleClass = 'page-scroller--visible';
  var enabledOffset = 60;
  var pageYOffset = 0;
  var inMemory = false;

  function resetPageScroller() {
    setTimeout(function () {
      if (window.pageYOffset > enabledOffset) {
        pageScroller.addClass(isVisibleClass);
      } else if (!pageScroller.hasClass(inMemoryClass)) {
        pageScroller.removeClass(isVisibleClass);
      }
    }, 150);

    if (!inMemory) {
      pageYOffset = 0;
      pageScroller.removeClass(inMemoryClass);
    }

    inMemory = false;
  }

  if (pageScroller.length > 0) {
    window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
      passive: true
    } : false);
    pageScroller.on('click', function (event) {
      event.preventDefault();
      window.removeEventListener('scroll', resetPageScroller);

      if (window.pageYOffset > 0 && pageYOffset === 0) {
        inMemory = true;
        pageYOffset = window.pageYOffset;
        pageScroller.addClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: 0
        }, 500, 'swing', function () {
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      } else {
        pageScroller.removeClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: pageYOffset
        }, 500, 'swing', function () {
          pageYOffset = 0;
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      }
    });
  }

  if ($('.c-steps').length) {
    $('.c-steps__buildings img').mapster({
      fillColor: '2979d4',
      stroke: false,
      fill: true,
      fillOpacity: 0.3,
      highlight: true,
      select: true,
      isSelectable: false,
      scaleMap: false
    });
    var pageX,
        pageX,
        left,
        top,
        left2,
        top2 = 0,
        areas = [],
        apartsAreas = []; // Подготоваливаем список областей

    var i = 0;
    $("#floorsmap area").each(function () {
      var $this = $(this);
      var key = $this.attr("floor"),
          title = $this.data('title'),
          count = $this.data('count'),
          desc = $this.data('desc'),
          free = $this.data('free'),
          item = {
        key: key,
        toolTip: "<div class='c-steps__popup-topline'><div class='c-steps__popup-topline-triangle'></div>" + title + "</div><div class='c-steps__popup-bottom'><div class='c-steps__popup-count'>" + count + "</div><div class='c-steps__popup-desc'>" + desc + "</div><div class='c-steps__popup-free'>" + free + "</div></div>",
        staticState: true,
        render_select: {
          fillOpacity: 0,
          fillColor: 'ffffff'
        }
      };
      areas.push(item);
    });
    $("#apartsmap area").each(function () {
      var $this = $(this);
      var key = $this.attr("aparts"),
          title = $this.data('title'),
          desc = $this.data('desc'),
          img = $this.data('img'),
          link = $this.data('link'),
          item = {
        key: key,
        toolTip: "<div class='c-steps__popup2-topline'><div class='c-steps__popup2-topline-triangle'></div>" + title + "</div><div class='c-steps__popup2-bottom'><div class='c-steps__popup2-desc'>" + desc + "</div><div class='c-steps__popup2-img'><img src='" + img + "' alt='' ></div><div class='c-steps__popup2-link'>" + link + "</div></div>",
        staticState: true,
        render_select: {
          fillOpacity: 0,
          fillColor: 'ffffff'
        }
      };
      apartsAreas.push(item);
    });
    $('.c-steps__floors-block-img img').mapster({
      fillColor: '8DC63F',
      stroke: false,
      fill: true,
      fillOpacity: 0.5,
      highlight: true,
      scaleMap: false,
      showToolTip: true,
      isSelectable: false,
      mapKey: "floor",
      // определяем ключевое поле
      onShowToolTip: function onShowToolTip(e) {
        var key = parseInt(e.key),
            left,
            top = 0;
        $('#floorsmap area').each(function (index, el) {
          var thisKey = $(this).attr('floor');

          if (key == thisKey) {
            var coords = $(this).attr('coords').split(',');
            left = coords[0] - 10;
            top = coords[coords.length - 1]; // top  = parseInt(top) + $('.c-steps__floors-block-img img').offset().top - 120;

            top = parseInt(top) + $('.c-steps__floors-block-img img').offset().top - 88;
          }
        });
        $('.c-steps__floors-block-item[data-floor=' + key + ']').addClass('active');
        e.toolTip.css({
          left: left + 'px',
          top: top + 'px'
        });

        if (key > 13) {
          $('.c-steps__popup-topline-triangle').css('top', top * 0.0132);
        } else if (key > 7) {
          $('.c-steps__popup-topline-triangle').css('top', top * 0.0232);
        } else {
          $('.c-steps__popup-topline-triangle').css('top', top * 0.0432);
        }
      },
      onStateChange: function onStateChange(e) {
        $('.c-steps__floors-block-item').removeClass('active');
      },
      toolTipContainer: '<div class="c-steps__popup"></div>',
      clickNavigate: true,
      areas: areas // Области

    });
    $('.c-steps__aparts-img img').mapster({
      fillColor: '8DC63F',
      stroke: false,
      fill: true,
      fillOpacity: 0.5,
      highlight: true,
      scaleMap: false,
      showToolTip: true,
      isSelectable: false,
      mapKey: "aparts",
      // определяем ключевое поле
      onShowToolTip: function onShowToolTip(e) {
        var key = parseInt(e.key),
            left,
            top = 0;
        $('#apartsmap area').each(function (index, el) {
          var thisKey = $(this).attr('aparts');

          if (key == thisKey) {
            var coords = $(this).attr('coords').split(',');
            left2 = coords[0] - 218;
            top2 = coords[coords.length - 1];
            console.log(top2); // top  = parseInt(top) + $('.c-steps__floors-block-img img').offset().top - 120;

            top2 = parseInt(top2) + $('.c-steps__aparts-img img').offset().top - 26;
          }
        });
        $('.c-steps__floors-block-item[data-floor=' + key + ']').addClass('active');
        e.toolTip.css({
          left: left2 + 'px',
          top: top2 + 'px'
        });
      },
      onStateChange: function onStateChange(e) {},
      toolTipContainer: '<div class="c-steps__popup2"></div>',
      clickNavigate: true,
      areas: apartsAreas // Области

    });
    $('.c-steps__step--first area, .c-steps__step--second area').on('click', function (event) {
      event.preventDefault();
      cchangeStep('next');
    });
    $('._js-stepsback').on('click', function (event) {
      event.preventDefault();
      cchangeStep('prev');
    });
    $(window).on('resize', function () {
      var mapWidth = $('.c-steps__steps').outerWidth();
      $('.c-steps__buildings img').mapster('resize', mapWidth);
      $('.c-steps__floors-block-img img').mapster('resize', mapWidth);
      $('.c-steps__aparts-img img').mapster('resize', mapWidth - 108);
    }).trigger('resize');
  }

  function cchangeStep(direction) {
    var direction = direction;
    var activeStep = $('.c-steps__step.active').data('step');
    $('.c-steps__step').removeClass('active').fadeOut();
    $('.c-steps__step').each(function (index, el) {
      index = index + 1;

      if (direction == 'next') {
        if (index == activeStep + 1) {
          $(this).addClass('active').fadeIn();
        }
      } else if (direction == 'prev') {
        if (index == activeStep - 1) {
          $(this).addClass('active').fadeIn();
        }
      } else {
        return;
      }
    });
  }

  $('#responsiveTabs').responsiveTabs({
    collapsible: false,
    navigationContainer: '.c-tabs__links-wrap',
    startCollapsed: false
  });
  var mainSlider = new Swiper(".tabsSwiper", {
    navigation: {
      nextEl: ".tabsSwiper .swiper-button-next",
      prevEl: ".tabsSwiper .swiper-button-prev"
    }
  });
});