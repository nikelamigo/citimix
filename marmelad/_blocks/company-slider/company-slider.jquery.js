if ($('.c-company-slider__swiper').length) {

	var buildSwiper = new Swiper(".c-company-slider__swiper", {
		slidesPerView: "auto",
		spaceBetween: 10,
		breakpoints: {
			481: {
				spaceBetween: 30
			},
		}
	});
}