if($('.c-content__progress-bar-per').length) {
	$('.c-content__progress-bar-per').each(function(index, el) {
		var percent = Number($(this).html())
		$(this).closest('.c-content__progress-bar').find('.c-content__progress-bar-line').css('width', percent + '%');
	});
}