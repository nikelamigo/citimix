if ($('.c-compare-slider__block').length) {

	var buildSwiper = new Swiper(".c-compare-slider__block", {
		slidesPerView: 3,
		spaceBetween: 0,
		breakpoints: {
			0: {
				slidesPerView: 1
			},
			769: {
				slidesPerView: 2
			},
			1201: {
				slidesPerView: 3
			},
		},
		navigation: {
			nextEl: ".c-compare-slider__slider-navs .swiper-button-next",
			prevEl: ".c-compare-slider__slider-navs .swiper-button-prev",
		},
	});

	// compareHeights();

	$('.c-compare-slider__item-compare-block').on('click', function(event) {
		event.preventDefault();
		$(this).toggleClass('active');
	});

}

function compareHeights () {

	$('.c-compare-slider__item-row--price').css('height', 'auto');
	var priceMaxHeight    = 0;
	var mortgageMaxHeight = 0;
	var settMaxHeight     = 0;
	var areaMaxHeight     = 0;
	var floarMaxHeight    = 0;

	$('.c-compare-slider__item').each(function(index, el) {
		var $this         = $(this);
		var priceHeight   = $this.find('.c-compare-slider__item-row--price').outerHeight();
		if(priceHeight > priceMaxHeight) {
			priceMaxHeight = priceHeight;
		}
	});
	$('.c-compare-slider__item-row--price').css('height', priceMaxHeight + 'px');
}

