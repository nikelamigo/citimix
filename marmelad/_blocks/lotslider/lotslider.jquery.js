if ($('.c-lotslider__block').length) {

	var lotSlider = new Swiper(".c-lotslider__block", {
		slidesPerView: 3,
		spaceBetween: 27,
		navigation: {
			nextEl: ".c-lotslider__slider-navs .swiper-button-next",
			prevEl: ".c-lotslider__slider-navs .swiper-button-prev",
		},
		breakpoints: {
			0: {
				slidesPerView: 1,
				spaceBetween: 20
			},
			769: {
				slidesPerView: 2,
				spaceBetween: 20
			},
			1321: {
				slidesPerView: 3,
				spaceBetween: 27
			}
		}
	});

}
