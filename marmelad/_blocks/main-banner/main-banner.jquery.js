function mainBannerPadding () {
	$('.c-main-banner').each(function(index, el) {
		var $this         = $(this),
			imgHeight     = $this.find('.c-main-banner__img').height(),
			contentOffset = $this.find('.c-main-banner__content').position().top,
			contentHeight = $this.find('.c-main-banner__content').outerHeight();


		$this.css('padding-bottom', (contentOffset + contentHeight) - imgHeight + 'px');


	});
}