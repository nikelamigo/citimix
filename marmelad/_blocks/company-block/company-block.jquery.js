if ($('.c-company-block__swiper').length) {
	var buildSwiper = new Swiper(".c-company-block__swiper", {
		navigation: {
			nextEl: ".c-company-block__navs .swiper-button-next",
			prevEl: ".c-company-block__navs .swiper-button-prev",
		}
	});
}