const maxHeight   = $('.app-header__middle').offset().top;
const blockHeight = $('.app-header__middle').outerHeight() + maxHeight;
var position      = $(window).scrollTop(); 

if (!window.matchMedia("(max-width: 1200px)").matches && position > maxHeight) {
  $(".app-header").addClass('scroller');
} else {
  $(".app-header").removeClass('scroller');
}


$(window).on('scroll', function(){

  if (!window.matchMedia("(max-width: 1200px)").matches) {
    let old_scroll = $(window).scrollTop();
    if (old_scroll > maxHeight) {
      $(".app-header").addClass('scroller');
    }
    else {
      $(".app-header").removeClass('scroller');
    }
  } else {
    
    var scroll = $(window).scrollTop();
    if (scroll > position || maxHeight >= scroll) {
      $(".app-header").removeClass('upscroll');
    } else {
      $(".app-header").addClass('upscroll');
    }
    if(scroll > blockHeight) {
      $('.app-header').addClass('not-visible').removeClass('visible');
    } else {
      $('.app-header').removeClass('not-visible').addClass('visible');
    }
    position = scroll;

  }
});


// когда шапка уедет за пределы видимости вещать на нее fixed с -100%