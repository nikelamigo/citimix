var callbackPopup = $('[data-remodal-id=callbackpopup]').remodal();

$('._js-callbackpopup').on('click', function(event) {
	event.preventDefault();
	callbackPopup.open();
});

$('.c-callbackpopup__close').on('click', function(event) {
	event.preventDefault();
	callbackPopup.close();
});