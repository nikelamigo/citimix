if($('.c-steps').length) {
    $('.c-steps__buildings img').mapster({
        fillColor: '2979d4',
        stroke: false,
        fill: true,
        fillOpacity: 0.3,
        highlight: true,
        select: true,
        isSelectable: false,
        scaleMap : false
    });

    var pageX, pageX, left, top, left2, top2 = 0,
            areas = [], apartsAreas = [];
        
        // Подготоваливаем список областей
        var i = 0;
        $("#floorsmap area").each(function(){
            var $this  = $(this);
            var key    = $this.attr("floor"),
                title  = $this.data('title'),
                count  = $this.data('count'),
                desc   = $this.data('desc'),
                free   = $this.data('free'),
                item   = {
                    key: key,
                    toolTip: "<div class='c-steps__popup-topline'><div class='c-steps__popup-topline-triangle'></div>"+ title +"</div><div class='c-steps__popup-bottom'><div class='c-steps__popup-count'>"+ count +"</div><div class='c-steps__popup-desc'>"+ desc +"</div><div class='c-steps__popup-free'>"+ free +"</div></div>",
                    staticState: true,
                    render_select: {
                        fillOpacity: 0,
                        fillColor: 'ffffff'
                    },
                };
                
            areas.push(item);
        });

        $("#apartsmap area").each(function(){
            var $this  = $(this);
            var key    = $this.attr("aparts"),
                title  = $this.data('title'),
                desc   = $this.data('desc'),
                img    = $this.data('img'),
                link   = $this.data('link'),
                item   = {
                    key: key,
                    toolTip: "<div class='c-steps__popup2-topline'><div class='c-steps__popup2-topline-triangle'></div>"+ title +"</div><div class='c-steps__popup2-bottom'><div class='c-steps__popup2-desc'>"+ desc +"</div><div class='c-steps__popup2-img'><img src='"+ img +"' alt='' ></div><div class='c-steps__popup2-link'>"+ link +"</div></div>",
                    staticState: true,
                    render_select: {
                        fillOpacity: 0,
                        fillColor: 'ffffff'
                    },
                };
                
            apartsAreas.push(item);
        });

    $('.c-steps__floors-block-img img').mapster({
        fillColor: '8DC63F',
        stroke: false,
        fill: true,
        fillOpacity: 0.5,
        highlight: true,
        scaleMap: false,
        showToolTip: true,
        isSelectable: false,
        mapKey: "floor", // определяем ключевое поле
        onShowToolTip: function(e) {
            var key  = parseInt(e.key),
                left, top  = 0;
            
            $('#floorsmap area').each(function(index, el) {
                var thisKey = $(this).attr('floor');
                if (key == thisKey) {
                    var coords = $(this).attr('coords').split(',');
                    left = coords[0] - 10;
                    top  = coords[coords.length - 1];
                    // top  = parseInt(top) + $('.c-steps__floors-block-img img').offset().top - 120;
                    top = parseInt(top) + $('.c-steps__floors-block-img img').offset().top - 88;

                }
            });
            $('.c-steps__floors-block-item[data-floor='+ key +']').addClass('active');
            
            e.toolTip.css({ left: left + 'px', top: top + 'px' });
            if(key > 13) {
                $('.c-steps__popup-topline-triangle').css('top', top * 0.0132);

            } else if (key > 7) {
                $('.c-steps__popup-topline-triangle').css('top', top * 0.0232);
            } else {
                $('.c-steps__popup-topline-triangle').css('top', top * 0.0432);
            }
            
        },
        onStateChange: function(e) {
            
            $('.c-steps__floors-block-item').removeClass('active');
        },
        toolTipContainer: '<div class="c-steps__popup"></div>',
        clickNavigate: true,
        areas: areas // Области
    });

    $('.c-steps__aparts-img img').mapster({
        fillColor: '8DC63F',
        stroke: false,
        fill: true,
        fillOpacity: 0.5,
        highlight: true,
        scaleMap: false,
        showToolTip: true,
        isSelectable: false,
        mapKey: "aparts", // определяем ключевое поле
        onShowToolTip: function(e) {
            var key  = parseInt(e.key),
                left, top  = 0;
            
            $('#apartsmap area').each(function(index, el) {
                var thisKey = $(this).attr('aparts');
                if (key == thisKey) {
                    var coords = $(this).attr('coords').split(',');
                    left2 = coords[0] - 218;
                    top2  = coords[coords.length - 1];
                    console.log(top2);
                    // top  = parseInt(top) + $('.c-steps__floors-block-img img').offset().top - 120;
                    top2 = parseInt(top2) + $('.c-steps__aparts-img img').offset().top - 26;

                }
            });
            $('.c-steps__floors-block-item[data-floor='+ key +']').addClass('active');
            
            e.toolTip.css({ left: left2 + 'px', top: top2 + 'px' });
            
        },
        onStateChange: function(e) {
            
        },
        toolTipContainer: '<div class="c-steps__popup2"></div>',
        clickNavigate: true,
        areas: apartsAreas // Области
    });

    $('.c-steps__step--first area, .c-steps__step--second area').on('click', function(event) {
        event.preventDefault();
        cchangeStep('next');
    });
    $('._js-stepsback').on('click', function(event) {
        event.preventDefault();
        cchangeStep('prev');
    });


    $(window).on('resize', function(){
        var mapWidth     = $('.c-steps__steps').outerWidth();
        $('.c-steps__buildings img').mapster('resize', mapWidth);
        $('.c-steps__floors-block-img img').mapster('resize', mapWidth);
        $('.c-steps__aparts-img img').mapster('resize', mapWidth - 108);
    }).trigger('resize');

}

function cchangeStep(direction) {
    var direction  = direction;
    var activeStep = $('.c-steps__step.active').data('step');
    $('.c-steps__step').removeClass('active').fadeOut();
    $('.c-steps__step').each(function(index, el) {
        index = index + 1;
        if(direction == 'next') {
            if(index == (activeStep + 1)) {
                $(this).addClass('active').fadeIn();
            }
        } else if(direction == 'prev') {
            if(index == (activeStep - 1)) {
                $(this).addClass('active').fadeIn();
            }
        } else {
            return
        }  
    });
}