
if ($('.b-stories__swiper').length) {
	var storiesSwiper = new Swiper(".b-stories__swiper", {
		slidesPerView: "auto",
		spaceBetween: 30,
	});
	
	var remodalSwiper = new Swiper("[data-remodal-id='extendedStories'] .swiper", {
		navigation: {
			nextEl: "[data-remodal-id='extendedStories'] .swiper-button-next",
			prevEl: "[data-remodal-id='extendedStories'] .swiper-button-prev",
		},
		autoplay: {
          delay: 15000,
          disableOnInteraction: false,
        }

	});

	$('.remodal .b-storie video').each(function(index, el) {
		var itemClass = 'palyer-'+index;
		$(this).addClass(itemClass);
		const player = new Plyr('.'+itemClass, {
			autoplay: false
		});



		remodalSwiper.on('slideChange', function () {
			player.pause();
			$('.b-storie__fake-video-length').removeClass('active');
			$('.fake-video-length-'+ remodalSwiper.activeIndex).addClass('active');
			$('.palyer-'+remodalSwiper.activeIndex).get(0).play();
		});

		$(document).on('closing', '.remodal', function (e) {
			player.pause();
			$('.b-storie__fake-video-length').removeClass('active');
		});

	});

	$('.b-storie__fake-video-length').each(function(index, el) {
		var itemClass = 'fake-video-length-'+index;
		$(this).addClass(itemClass);
	});


	var extendedStories = $('[data-remodal-id=extendedStories]').remodal();
	    

	$('.b-stories .b-storie__play').on('click', function(event) {
		event.preventDefault();
		extendedStories.open();
		// console.log($(this).closest('.b-storie').index());
		remodalSwiper.slideTo($(this).closest('.b-storie').index());
		if($('.palyer-'+$(this).closest('.b-storie').index()).length) {
			$('.palyer-'+$(this).closest('.b-storie').index()).get(0).play();
		}
		if($('.fake-video-length-'+ $(this).closest('.b-storie').index()).length) {
			$('.fake-video-length-'+ $(this).closest('.b-storie').index()).addClass('active');
		}
	});


}


