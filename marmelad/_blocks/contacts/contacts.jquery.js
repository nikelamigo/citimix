(function(){
    if (!$('#contactsMap').length) {
        return;
    }
    var myMap;
    var myPlacemark;
    ymaps.ready(init);

    function init () {

        // Создание экземпляра карты.
        myMap = new ymaps.Map('contactsMap', {
            center: [55.664334, 37.465928],
            zoom: 15,
            controls: []
        });

        myMap.behaviors.disable('scrollZoom');

        myPlacemark = new ymaps.Placemark([55.664334, 37.465928], {
            hintContent: ''
        }, {
            iconLayout: 'default#image',
            iconImageHref: '../static/img/map1.png',
            iconImageSize: [138, 138]

        });

        myMap.geoObjects
            .add(myPlacemark);
    }
})();