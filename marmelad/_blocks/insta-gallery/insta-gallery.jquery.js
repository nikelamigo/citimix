if($('.c-insta-gallery').length) {
	var leftMargin = $('.c-insta-gallery__title').offset().left;
	$('.c-insta-gallery__items').css('margin-left', leftMargin + 'px');

	$(window).on('resize', function(event) {
		leftMargin = $('.c-insta-gallery__title').offset().left;
		$('.c-insta-gallery__items').css('margin-left', leftMargin + 'px');	
	});
	$('.c-insta-gallery__items').addClass('owl-carousel').owlCarousel({
		margin: 30,
		nav: false,
		dots: false,
		autoWidth:true,
		mouseDrag      : true,
		touchDrag      : true,
	});
}