// $('.c-main-slider__items').addClass('owl-carousel').owlCarousel({
// 	items          : 1,
// 	autoplay       : false,
// 	loop           : true,
// 	mouseDrag      : false,
// 	touchDrag      : true,
// 	animateOut     : 'fadeOut',
// 	nav            : true,
// 	navText        : ['<svg xmlns="http://www.w3.org/2000/svg" width="14" height="26" viewBox="0 0 14 26" fill="none"><path d="M1.77036 25.3287C1.59671 25.5024 1.37966 25.5892 1.1409 25.5892C0.90214 25.5892 0.685085 25.5024 0.511442 25.3287C0.164155 24.9814 0.164155 24.4171 0.511442 24.0698L11.5812 13L0.511442 1.93027C0.164155 1.58298 0.164155 1.01864 0.511442 0.671354C0.858729 0.324067 1.42307 0.324067 1.77036 0.671354L13.4696 12.3706C13.8169 12.7179 13.8169 13.2822 13.4696 13.6295L1.77036 25.3287Z"/></svg>','<svg xmlns="http://www.w3.org/2000/svg" width="14" height="26" viewBox="0 0 14 26" fill="none"><path d="M1.77036 25.3287C1.59671 25.5024 1.37966 25.5892 1.1409 25.5892C0.90214 25.5892 0.685085 25.5024 0.511442 25.3287C0.164155 24.9814 0.164155 24.4171 0.511442 24.0698L11.5812 13L0.511442 1.93027C0.164155 1.58298 0.164155 1.01864 0.511442 0.671354C0.858729 0.324067 1.42307 0.324067 1.77036 0.671354L13.4696 12.3706C13.8169 12.7179 13.8169 13.2822 13.4696 13.6295L1.77036 25.3287Z""/></svg>'],
// 	dots           : false
// });

var mainSlider = new Swiper(".c-main-slider__items", {
	
	navigation: {
		nextEl: ".c-main-slider__navs .swiper-next",
		prevEl: ".c-main-slider__navs .swiper-prev",
	}
});