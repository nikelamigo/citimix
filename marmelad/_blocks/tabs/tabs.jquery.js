$('#responsiveTabs').responsiveTabs({
    collapsible: false,
    navigationContainer: '.c-tabs__links-wrap',
    startCollapsed: false
});
var mainSlider = new Swiper(".tabsSwiper", {
    
    navigation: {
        nextEl: ".tabsSwiper .swiper-button-next",
        prevEl: ".tabsSwiper .swiper-button-prev",
    }
});