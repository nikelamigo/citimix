if($('.c-lot__sliders-wrap').length) {
	$('.c-lot__sliders').each(function(index, el) {
		var indexSlider = index;
		$(this).addClass('slider' + indexSlider);

		var swiper = new Swiper('.c-lot__sliders.slider' + indexSlider + ' .c-lot__thumb-slider', {
			direction: "vertical",
			slidesPerView: 4,
			watchSlidesProgress: true,
			watchSlidesVisibility: true,
			watchOverflow: true,
			spaceBetween: 18,
			breakpoints: {
	          0: {
	            slidesPerView: 3
	           },
	          1360: {
	            slidesPerView: 4
	           }
	        }
		});


		var swiper2 = new Swiper('.c-lot__sliders.slider' + indexSlider +' .c-lot__main-slider', {
			watchOverflow: true,
			watchSlidesVisibility: true,
			watchSlidesProgress: true,
			preventInteractionOnTransition: true,
			navigation: {
			  nextEl: ".c-lot__sliders.slider" + indexSlider + " .swiper-button-next",
			  prevEl: ".c-lot__sliders.slider" + indexSlider + " .swiper-button-prev"
			},
			thumbs: {
			  swiper: swiper,
			},
			effect: 'fade',
			    fadeEffect: {
			    crossFade: true
			}
		});
	});
}
$('.c-lot__fav').on('click', function(event) {
	event.preventDefault();
	$(this).toggleClass('active');
});
$('.c-lot__slider-link a').on('click', function(event) {
	event.preventDefault();
	var $this = $(this);
	var id    = $this.attr('href');
	console.log(id);

	if($this.hasClass('active')) {
		return
	} else {
		$('.c-lot__sliders').removeClass('active');
		$('.c-lot__slider-link a').removeClass('active');
		$this.addClass('active');	
		$(id).addClass('active');
	}

});