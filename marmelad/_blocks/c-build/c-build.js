

if ($('.c-build__swiper').length) {

	var buildSwiper = new Swiper(".c-build__swiper", {
		slidesPerView: "auto",
		spaceBetween: 10,
		breakpoints: {
			1025: {
				slidesPerView: 2,
				spaceBetween: 30
			},
		}
	});

	var buildGallery = $('[data-remodal-id=buildGallery]').remodal();

	$('.c-build__swiper-item').on('click', function(event) {
		event.preventDefault();

		var popupGallery = $(this).find('.c-popup').clone();
		$('[data-remodal-id="buildGallery"] .remodal-content').html(popupGallery);

		var popupSwiper = new Swiper(".c-popup", {
			navigation: {
				nextEl: ".c-popup .swiper-button-next",
				prevEl: ".c-popup .swiper-button-prev",
			},
		});
		buildGallery.open();
	});


	$(document).on('closed', '.remodal[data-remodal-id=buildGallery]', function (e) {
		$('[data-remodal-id="buildGallery"] .remodal-content').html('');
	});

}



