$('.app-footer__menu > li, .app-footer__design-wrap > li').each(function(index, el) {
	if($(this).find('>ul').length){
		$(this).find('>a').append('<span class="menu-trigger"><svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.81323 5.6762L1.76913 0.176209L0.474 1.35479L5.22289 5.67627L0.474131 9.99764L1.76928 11.1762L7.81323 5.6762Z"></path></svg></span>')
	}
});
$(document).on('click', '.app-footer__menu .menu-trigger', function(event) {
	event.preventDefault();
	$(this).closest('li').toggleClass('open').find('>ul').slideToggle();
});