;(function() {

    $.preventScrolling = function(selector, options) {

        // запрещаем прокрутку страницы при прокрутке элемента
        var defaults = {

            classes : {
                scrolled : 'is-scrolled',
                onTop    : 'is-onTop',
                onBottom : 'is-onBottom',
            },
            onTop    : function() {},
            onBottom : function() {}
        };

        var options = $.extend({}, defaults, options);

        var scroller = $(selector);

        scroller.on('scroll', function() {

            if (scroller.scrollTop() == 0) {
                scroller
                    .addClass(options.classes.onTop)
                    .removeClass(options.classes.onBottom);
            }

            if (scroller.scrollTop() == (scroller[0].scrollHeight - scroller.height())) {
                scroller
                    .removeClass(options.classes.onTop)
                    .addClass(options.classes.onBottom);
            }
        });

        if (scroller[0].scrollHeight > scroller.height()) {
            scroller.addClass('with-scroll');
        } else {
            scroller.removeClass('with-scroll');
        }

        $(window).on('resize', function() {

            if (scroller[0].scrollHeight > scroller.height()) {
                scroller.addClass('with-scroll');
            } else {
                scroller.removeClass('with-scroll');
            }
        });

        scroller.off('mousewheel DOMMouseScroll').on('mousewheel DOMMouseScroll', function(e) {

            var scrollTo = null;

            if (e.type == 'mousewheel') {
                scrollTo = (e.originalEvent.wheelDelta * -1);
            } else if (e.type == 'DOMMouseScroll') {
                scrollTo = 40 * e.originalEvent.detail;
            }

            if (scrollTo && scroller[0].scrollHeight > scroller.height()) {
                e.stopPropagation();
                e.preventDefault();
                $(this).scrollTop(scrollTo + $(this).scrollTop());
            }
        });
    };

})();

/**
 * Simple menu
 * @version 1.0.0-beta.1
 */
;(function($) {

  $.fn.simpleMenu = function(options) {

    var settings = $.extend(true, {
        timing : 300,
        topMargin : -6,
        menu   : {
            list    : 'ul',
            item    : 'li',
            trigger : 'a'
        },
        classes : {
            opened : 'opened',
            active : 'active',
            used   : 'used'
        },
        attrs : {
            opened : {
                key    : 'opened',
                true   : 'true',
                false  : 'false'
            }
        }
    }, options);

    var $this = this;
    var $trigers = $this.find(settings.menu.list).parent(settings.menu.item).find('> ' + settings.menu.trigger);

    $trigers.on('click', function(event) {

        event.preventDefault();

        var $list = $(this).parent(settings.menu.item).find('> ' + settings.menu.list);

        $list.css({
            display:'block',
        });

        if ($list.parent(settings.menu.item).hasClass(settings.classes.opened)) {

            $list.stop().animate({
                marginTop : -($list.outerHeight(true)-settings.topMargin)
            }, settings.timing, function() {
                $list
                    .attr(settings.attrs.opened.key, settings.attrs.opened.false)
                    .addClass(settings.classes.used)
                    .parent(settings.menu.item).removeClass(settings.classes.opened);
            });

        } else {

            if (!$list.hasClass(settings.classes.used)) {
                $list
                    .css({
                        marginTop : -($list.outerHeight(true)-settings.topMargin)
                    })
                    .addClass(settings.classes.used);
            }

            $list
                .parent(settings.menu.item).addClass('opening')
                .end()
                .stop().animate({
                    marginTop : (0 + settings.topMargin)
                }, settings.timing, function() {
                    $list
                        .attr(settings.attrs.opened.key, settings.attrs.opened.true)
                        .parent(settings.menu.item).removeClass('opening')
                            .end()
                        .addClass(settings.classes.used)
                        .parent(settings.menu.item).addClass(settings.classes.opened);
                });
        }

    });

  };

})(jQuery);

/**
 * Мобильное меню сайта
 */
var asideMenuBtn      = $('.l-aside-menu-btn');
var asideMenu         = $('.l-aside-menu');
var asideHead         = $('.l-aside-menu__head');
var asideMenuContent  = $('.l-aside-menu__content');
var asideMenuScroller = $('.l-aside-menu__scroller-content');
var asideMenuFoot     = $('.l-aside-menu__foot');


function openAsideMenu() {
    asideMenuBtn.addClass('js-opening');
    asideMenu.addClass('js-animate js-opening');
    $('html').addClass('menu-fixed');
    $('body').addClass('menu-fixed').css('padding-right', getScrollbarWidth() + 'px');
}

function closeAsideMenu() {

    asideMenuBtn.removeClass('js-opening');
    asideMenu.removeClass('js-animate');

    setTimeout(function() {
        asideMenu.removeClass('js-opening');
        $('html').removeClass('menu-fixed');
        $('body').removeClass('menu-fixed').css('padding-right', 0);
    }, 150);
}


asideMenuBtn.on('pointerup', function(event) {
    event.preventDefault();
    if($(this).hasClass('js-opening')) {
        closeAsideMenu();
    } else {
        openAsideMenu();
    }
});

/**
 * запрещаем прокрутку страницы при прокрутке бокового-мобильного
 */
$.preventScrolling($('.l-aside-menu__scroller'));



if ($.exists('.app-header__icons')) {

    var newMainMenu    = $('.app-header__icons').clone();


    newMainMenu
        .addClass('app-header__icons--adaptive')
        .appendTo(asideMenuScroller);


}


/**
 * Клонирование верхнего-левого меню в боковое-мобильное
 */

if ($.exists('.app-header__menu')) {

    var newMainMenu    = $('.app-header__menu').clone();


    newMainMenu
        .removeClass('app-header__menu')
        .addClass('aside-nav-list aside-nav-list__menu')
        .appendTo(asideMenuScroller);


}

/*Добавление стрелочек для li*/
$.each(asideMenuScroller.find('li'), function(index, element) {

    if ($(element).find('ul').length) {

        var triggerIcon = ['<div class="svg-icon svg-icon--angle-down">',
                '<svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">',
                    '<path d="M7.81323 5.6762L1.76913 0.176209L0.474 1.35479L5.22289 5.67627L0.474131 9.99764L1.76928 11.1762L7.81323 5.6762Z" fill="black"/>',
                '</svg>',
            '</div>'].join('');

        var subMenuTrigger = $('<div class="sub-menu-trigger">' + triggerIcon + '</div>');

        $(element)
            .addClass('is-has-child')
            .append(subMenuTrigger);
    }
});
if ($.exists('.aside-nav-list')) {

    $('.aside-nav-list').simpleMenu({
        timing : 500,
        menu : {
            trigger : '.sub-menu-trigger'
        }
    });
}