(function(){
    if (!$('#mapBlock').length) {
        return;
    }
    var myMap;
    var myPlacemark;
    var myMapWithoutPopup = $('#mapBlock').hasClass('c-map__block--without-popup');
    var mapPopup = $('[data-remodal-id=map]').remodal();
    ymaps.ready(init);

    function init () {

        // Создание экземпляра карты.
        myMap = new ymaps.Map('mapBlock', {
            center: [55.664334, 37.465928],
            zoom: 15,
            controls: []
        });

        myMap.behaviors.disable('scrollZoom');

        myPlacemark = new ymaps.Placemark([55.664334, 37.465928], {
            hintContent: ''
        }, {
            iconLayout: 'default#image',
            iconImageHref: '../static/img/map1.png',
            iconImageSize: [138, 138]

        });

        myMap.geoObjects
            .add(myPlacemark);

        // var balloonLayout = ymaps.templateLayoutFactory.createClass(
        //             '<div class="b-marker">'
        //             +'<div class="b-marker__item">'
        //             +'<div class="svg-icon svg-icon--location" aria-hidden="true"><svg class="svg-icon__link"><use xlink:href="#location"></use></svg></div>'
        //             +'{{ properties.title }}'
        //             +'</div>'
        //             +'<div class="b-marker__item">'
        //             +'<div class="svg-icon svg-icon--clock" aria-hidden="true"><svg class="svg-icon__link"><use xlink:href="#clock"></use></svg></div>'
        //             +'{{ properties.time }}'
        //             +'</div>'
        //             +'<div class="b-marker__item">'
        //             +'<div class="svg-icon svg-icon--phone" aria-hidden="true"><svg class="svg-icon__link"><use xlink:href="#phone"></use></svg></div>'
        //             +'<a href="tel:{{ properties.phonen }}">{{ properties.phone }}</a>'
        //             +'</div>'
        //             +'<a href="{{ properties.detail }}" class="btn">'
        //             +'Как добраться'
        //             +'</a>'
        //             +'</div>'
        //         , {

        //             build: function () {
        //                 this.constructor.superclass.build.call(this);
        //                 this._$element = $('.contacts__marker', this.getParentElement());
        //                 this._$element.find('.close')
        //                 .on('click', $.proxy(this.onCloseClick, this));
        //             },
        //             onCloseClick: function (e) {
        //                 e.preventDefault();
        //                 this.events.fire('userclose');
        //             },
        //             getShape: function () {
        //                 var el = this.getElement(),
        //                     result = null;
        //                 if (el) {
        //                     var firstChild = el.firstChild;
        //                     result = new ymaps.shape.Rectangle(
        //                         new ymaps.geometry.pixel.Rectangle([
        //                             [0, 0],
        //                             [firstChild.offsetWidth, firstChild.offsetHeight]
        //                         ])
        //                     );
        //                 }
        //                 return result;
        //             }
        //         }
        //     );

        function addCollection(index, el) {
            var $this      = $(el);
            var collection = new ymaps.GeoObjectCollection({},{
                preset: ""
            }); 
            var icon       = $this.data('icon');
            var hoverIcon  = $this.data('hover-icon');
            $this.find('.c-map__block-popup-point').each(function(index, el) {
                var latLang     = $(el).data('coords').split(',');
                var imgSrc      = $(el).data('img');
                var title       = $(el).data('title');
                var description = $(el).data('description');
                var link        = $(el).data('link'); 

                var placemark   = new ymaps.Placemark([Number(latLang[0]),Number(latLang[1])], { 
                    title: title,
                    description: description,
                    imgSrc: imgSrc,
                    link: link,
                    defaultIcon : icon,
                    hoverIcon: hoverIcon
                    }, {
                    //balloonLayout: balloonLayout,
                    //balloonPanelMaxMapArea: 0,
                    // hintLayout: HintLayout,
                    hideIconOnBalloonOpen: false,
                   iconLayout: 'default#imageWithContent',
                   iconImageHref: icon,
                   iconImageSize: [42, 50],
                   iconImageOffset: [0, 0]
                })

                collection.add(placemark);
                if(!myMapWithoutPopup) {
                    placemark.events.add('mouseenter', function(e){
                        var hoverIcon = e.get('target').properties.get('hoverIcon');
                        e.get('target').options.set('iconImageHref', hoverIcon);
                    });
                    placemark.events.add('mouseleave', function(e){
                        var defaultIcon = e.get('target').properties.get('defaultIcon');
                        e.get('target').options.set('iconImageHref', defaultIcon);
                    });
                    placemark.events.add('click', function(e){
                        var popup = $('.c-map__placemark-popup');
                        
                        var title       = e.get('target').properties.get('title');
                        var imgSrc      = e.get('target').properties.get('imgSrc');
                        var description = e.get('target').properties.get('description');
                        var link        = e.get('target').properties.get('link');

                        popup.find('.c-map__placemark-popup-title').text(title);
                        popup.find('.c-map__placemark-popup-img img').attr('src', imgSrc);
                        popup.find('.c-map__placemark-popup-description').text(description);
                        popup.find('.c-map__placemark-popup-link > a').attr('href', link);

                        if (window.matchMedia("(max-width: 1200px)").matches) {
                            var placemarkPopup = popup.clone();
                            $('[data-remodal-id="map"] .remodal-content').html(placemarkPopup);
                            mapPopup.open();
                        } else {
                            popup.addClass('active');
                        }

                    })
                }
            });
            myMap.geoObjects.add(collection);
            $this.on('click', function(event) {
                event.preventDefault();
                if($(this).hasClass('disabled')) {
                    $(this).removeClass('disabled');
                    myMap.geoObjects.add(collection);
                }  else {
                    $(this).addClass('disabled');
                    myMap.geoObjects.remove(collection);
                }
            });
        }

        $('.c-map__block-popup-items.active .c-map__block-popup-item').each(addCollection);


        $('.c-map__link').on('click', function(event) {
            event.preventDefault();
            var $this = $(this);
            if($this.hasClass('active')) {
                return
            }
            myMap.geoObjects.removeAll();
            myMap.geoObjects.add(myPlacemark);


            var role  = $this.data('role');
            $('.c-map__link').removeClass('active');
            $this.addClass('active');

            $('.c-map__block-popup-items.active .c-map__block-popup-item').off('click');
            $('.c-map__block-popup-items').removeClass('active');
            $('.c-map__placemark-popup').removeClass('active');
            $('.c-map__block-popup-items[data-role="'+ role +'"]').addClass('active');

            $('.c-map__block-popup-items.active .c-map__block-popup-item').each(addCollection);

            // myMap.geoObjects.each(function(obj, i){
            //     if (obj instanceof ymaps.Placemark) {
            //         return
            //     }
            //     myMap.geoObjects.remove(obj);
            // });


        });

    }

    $(document).on('closed', '.remodal[data-remodal-id=map]', function (e) {
        $('[data-remodal-id="map"] .remodal-content').html('');
    });
})();