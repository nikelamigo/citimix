$('.c-apartment__filter-item-button').on('click', function(event) {
	event.preventDefault();
	$(this).toggleClass('active');
});
$('.custom-range').each(function(index, el) {
    var $this = $(this),
    dataStep      = $this.data('step'),
    dataDecimals  = $this.data('decimals'),
    $lower        = $this.closest('.custom-range').find('.custom-range__low'),
    $upper        = $this.closest('.custom-range').find('.custom-range__high'),
    inputLowVal   = $lower.val().replace(',', '.'),
    inputHighVal  = $upper.val().replace(',', '.');
    var inputRangeMax = $upper.data('val').toString().replace(',', '.');    
    var inputRangeMin = $lower.data('val').toString().replace(',', '.');

    var arr = [Number(inputRangeMin), Number(inputRangeMax)];

    $lower.on('input', function(event) {
      this.value = this.value.replace(/[^0-9,.]/g,'');
      var value  = $(event.target).val().replace(',', '.');
      slider.val([value, null]);
    });
    $upper.on('input', function(event) {
      this.value = this.value.replace(/[^0-9,.]/g,'');
      var value  = $(event.target).val().replace(',', '.');
      slider.val([null, value]);
    });
    var slider = $this.find('.custom-range__block').noUiSlider({
    	start: [inputLowVal, inputHighVal],
    	connect: true,
    	behaviour: 'drag-tap',
    	format: wNumb({
          decimals: dataDecimals
        }),
        range: {
          'min': [arr[0]],
          'max': [arr[1]]
        },
        step: dataStep
    });

    // $lower.on('input', function(event) {
    // 	slider.val([$(event.target).val(), null]);
    // });
    // $upper.on('input', function(event) {
    //   slider.val([null, $(event.target).val()]);
    // });

    // slider.Link('lower').to($lower);
    // slider.Link('upper').to($upper);
    slider.on('set', function(values, handle) {
    	let lowVal = parseFloat(slider.val()[0]);
    	if(lowVal == inputRangeMin) {
    		$this.addClass('custom-range__border')
    	} else {
    		$this.removeClass('custom-range__border')
    	}
    });
    slider.on('slide', function(values, handle) {
    	$lower.val(handle[0].replace('.', ','));
    	$upper.val(handle[1].replace('.', ','));
    	let lowVal = parseFloat(slider.val()[0]);
    	if(lowVal == inputRangeMin) {
    		$this.addClass('custom-range__border')
    	} else {
    		$this.removeClass('custom-range__border')
    	}
    });
});

$('.custom-range--one').each(function(index, el) {
    var $this     = $(this),
    dataStep      = $this.data('step'),
    dataDecimals  = $this.data('decimals'),
    $upper        = $this.closest('.custom-range--one').find('.custom-range__high'),
    inputHighVal  = $upper.val(),
    inputRangeMax = $upper.data('val').toString().replace(',', '.');

    $upper.val(numberWithSpaces(inputHighVal));

    inputHighVal  = inputHighVal.replace(',', '.');
    inputRangeMax = inputRangeMax.replace(',', '.');

    $upper.on('input', function(event) {
      this.value = numberWithSpaces(this.value.replace(/[^0-9,.]/g,''));
      var value = $(event.target).val().replace(',','.'); 
      slider.val(value);
    });


    var slider = $this.find('.custom-range__block').noUiSlider({
      start: inputHighVal,
      connect: 'lower',
      behaviour: 'drag-tap',
      format: wNumb({
          decimals: dataDecimals
        }),
      range: {
          'min': 0,
          'max': Number(inputRangeMax)
      },
      step: dataStep
    });

    // $upper.on('input', function(event) {
    //   var value = $(event.target).val().replace(',','.'); 
    //   slider.val(value);
    // });

    slider.on('slide', function(values, handle) {
      $upper.val(numberWithSpaces(handle.replace('.', ',')));
      let lowVal = parseFloat(slider.val()[0]);
      if(lowVal == 0) {
        $this.addClass('custom-range__border')
      } else {
        $this.removeClass('custom-range__border')
      }
    });

    // slider.Link('lower').to($lower);
    // slider.Link('upper').to($upper);
});
if($('.c-apartment--selectbypar').length) {
  var filterPopup = $('[data-remodal-id=filterRemodal]').remodal();
  var filterHtml  = $(this).find('.c-apartment__block').clone(true);
  $('[data-remodal-id="filterRemodal"] .remodal-content').html(filterHtml);
  $('._js-filter-remodal').on('click', function(event) {
    event.preventDefault();
    filterPopup.open();
    $('select').trigger('refresh');
  });
}