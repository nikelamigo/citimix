/* ^^^
 * Глобальные-вспомогательные функции
 * ========================================================================== */

/* ^^^
 * Viewport Height Correction
 *
 * @link https://www.npmjs.com/package/postcss-viewport-height-correction
 * ========================================================================== */
function setViewportProperty(){
  var vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', vh + 'px');
}
window.addEventListener('resize', setViewportProperty);
setViewportProperty(); // Call the fuction for initialisation

/* ^^^
 * Возвращает HTML-код иконки из SVG-спрайта
 *
 * @param {String} name Название иконки из спрайта
 * @param {Object} opts Объект настроек для SVG-иконки
 *
 * @example SVG-иконка
 * getSVGSpriteIcon('some-icon', {
 *   tag: 'div',
 *   type: 'icons', // colored для подключения иконки из цветного спрайта
 *   class: '', // дополнительные классы для иконки
 *   mode: 'inline', // external для подключаемых спрайтов
 *   url: '', // путь до файла спрайта, необходим только для подключаемых спрайтов
 * });
 */
function getSVGSpriteIcon(name, opts) {
  opts = Object.assign({
    tag: 'div',
    type: 'icons',
    class: '',
    mode: 'inline',
    url: '',
  }, opts);

  let external = '';
  let typeClass = '';

  if (opts.mode === 'external') {
    external = `${opts.url}/sprite.${opts.type}.svg`;
  }

  if (opts.type !== 'icons') {
    typeClass = ` svg-icon--${opts.type}`;
  }

  opts.class = opts.class ? ` ${opts.class}` : '';

  return `
    <${opts.tag} class="svg-icon svg-icon--${name}${typeClass}${opts.class}" aria-hidden="true" focusable="false">
      <svg class="svg-icon__link">
        <use xlink:href="${external}#${name}"></use>
      </svg>
    </${opts.tag}>
  `;
}

/* ^^^
 * JQUERY Actions
 * ========================================================================== */
$(function() {

  'use strict';

  /**
   * определение существования элемента на странице
   */
  $.exists = (selector) => $(selector).length > 0;

  function getScrollbarWidth() {
      var outer = document.createElement("div");
      outer.style.visibility = "hidden";
      outer.style.width = "100px";
      outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps

      document.body.appendChild(outer);

      var widthNoScroll = outer.offsetWidth;
      // force scrollbars
      outer.style.overflow = "scroll";

      // add innerdiv
      var inner = document.createElement("div");
      inner.style.width = "100%";
      outer.appendChild(inner);        

      var widthWithScroll = inner.offsetWidth;

      // remove divs
      outer.parentNode.removeChild(outer);

      return widthNoScroll - widthWithScroll;
  };

  const threedPopup    = $('[data-remodal-id=3dpopup]').remodal();
  $('._js-3dpopup').on('click', function(event) {
    event.preventDefault();
    threedPopup.open();
  });

  function numberWithSpaces(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    return parts.join(".");
  }

  $('.masked-input').attr('placeholder','+7(___) ___-__-__').mask("+7(999) 999-99-99");

  /*sizeCover*/

;(function () {
    var bgCover = $('[data-section-bg]');

    if (!$.exists(bgCover)) {
        return;
    }

    var cover = function cover() {
        var self = $(this);
        var selfBg = self.data('section-bg');

        if (selfBg) {
            self.css('background-image', 'url(' + selfBg + ')');
        }
    };

    $.each(bgCover, cover);
})();

var flag768    = false;
var flag480    = false;
var stepsBlock = $('.c-steps').length; 

$(window).on('resize', function(){
  $('select').trigger('refresh');
  mainBannerPadding();
  // if(stepsBlock) {
  //   var mapWidth  = $('body').outerWidth();
  //   console.log(mapWidth);
  //   $('.c-steps__buildings img').mapster('resize', mapWidth);
  // }

  if (window.matchMedia("(max-width:768px)").matches){
    if (!flag768){
      if($('.c-apartments__content-layout-button').length) {
        $('.c-apartments__content-layout-button').appendTo('.c-apartments__content-layout--right');
      }

      flag768 = true;
    }
  } else {
    if (flag768){
      if($('.c-apartments__content-layout-button').length) {
        $('.c-apartments__content-layout-button').appendTo('.c-apartments__content-layout--left');
      }

      flag768 = false;
    }
  }
  // Удалить при заливке
  if (window.matchMedia("(max-width:480px)").matches){
    if (!flag480){
      if($('.c-mortgage').length) {
        $('.c-mortgage__banks-elem').each(function(index, el) {
          if(index > 1) {
            $(el).hide();
          }
        });
      }

      flag480 = true;
    }
  } else {
    if (flag480){
      if($('.c-mortgage').length) {
        $('.c-mortgage__banks-elem').each(function(index, el) {
          
            $(el).show();
        });
      }

      flag480 = false;
    }
  }
}).trigger('resize');

  // function commaDot(str) {
  //   var str = str.toString().split('.');
  //   str[0]  = str[0].replace(/,/g, '.');
  //   return str.join('.');
  // }
  // function dotComma(str) {
  //   var str = str.toString().replace(/./g, ',');
  //   return str;
  // }

  //=require ../_blocks/**/*.js
});
